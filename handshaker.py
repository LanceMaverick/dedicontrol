import requests
from time import sleep
import json
import logging
from config import Config


assert(Config.key is not None)
assert(Config.polling >= 6000)


key = Config.key
server_id = Config.server_id
endpoint = Config.api_endpoint
logging.basicConfig(filename='handshaker.log',level=getattr(logging, Config.log_level))


while True:
    try:
        r_get = requests.get('https://api.ipify.org')
        assert r_get.status_code == 200
    except AssertionError:
        logging.error("Could not find ip with ipify. Status code {}".format(r_get.status_code))

    else:
        ip = r_get.text
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        data = {'ip':ip, 'key':key, 'uid':server_id}

        try:
            r_put = requests.put(endpoint, data=json.dumps(data), headers=headers)
            assert r_put.status_code == 200
        except AssertionError:
            logging.error("Could not update IP on nameserver. [Error code {}]".format(r_put.status_code))
        else:
            logging.debug("The IP has been updated to {}".format(ip))

    sleep(Config.polling)
